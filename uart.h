#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <stdlib.h>

#define SLCT_TI 0xC1
#define SLCT_TR 0xC2
#define USR_CMD 0xC3

#define SIGNAL_CTRL_SNDR    0xD1
#define SIGNAL_REF_SNDR     0xD2
#define SIS_STATE_SNDR      0xD3
#define CTRL_TEMP_MODE      0xD4
#define BEHAVIOR_MODE_SNDR  0xD5
#define TEMPA_SNDR          0xD6

short CRC16(short crc, char data);

short calcula_CRC(unsigned char *commands, int size);

int printBuffer(unsigned char *buf, int size);

int reqData(int fileStream, int dataType);

int sendData(int fileStream, int dataType, char *data, int size);

int readData(int fileStream, char *bufferRetorno, int size);