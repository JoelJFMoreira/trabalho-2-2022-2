#include <stdio.h>
#include <wiringPi.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <softPwm.h>

#include "pid.h"
#include "uart.h"
#include "bme280.h"

#define RESIST  4
#define VENT    5

#define I2C_ADDR 0x76

pthread_t t_controleDash;
pthread_t t_controlePID;

float tempInt = 0;
float tempRef = 0;
float tempExt = 0;
float PWMrest = 0;
float PWMvent= 0;

float kp = 30;
float ki = 0.2;
float kd = 400;

int uartData = -1;
int estado = 0;
int forno = 0;

int setupUart(){
    int filestream = -1;

    filestream = open("/dev/serial0", O_RDWR | O_NOCTTY | O_NDELAY);
    
    if (filestream == -1){
        printf("Erro - Não foi possível iniciar a UART.\n");
    }
    else{
        printf("UART inicializada!\n");
    }

    return filestream;
}

void setupProgram(){
    uartData = setupUart();
    if(wiringPiSetup() == -1){
        exit(1);
    }

    pinMode(RESIST, OUTPUT);
    pinMode(VENT, OUTPUT);

    softPwmCreate(RESIST,0,100);
    softPwmCreate(VENT,0,100);

    pid_configura_constantes(kp,ki,kd);

}

void trataSinal(int signal){
    int encerra = 0;
    char buffer[4];
    printf("Encerrando...\n");

    memcpy(&buffer, (char*)&encerra, sizeof(encerra));
    sendData(uartData, SIS_STATE_SNDR, buffer, 1);

    pthread_cancel(t_controleDash);
    pthread_cancel(t_controlePID);

    softPwmWrite(RESIST,0);
    softPwmWrite(VENT,0);
    exit(0);
}

void getTemp(){
    char buffer[40];
    while(1){
        reqData(uartData, SLCT_TI);
        usleep(1000000);
        readData(uartData, buffer, 9);
        memcpy(&tempInt, &buffer[3], 4);
        reqData(uartData, SLCT_TR);
        usleep(1000000);
        readData(uartData, buffer, 9);
        memcpy(&tempRef, &buffer[3], 4);

    }
}

void setResist(float valorResistor){
    int max = 100;
    char temp[4];
    int auxResist=0;

    memcpy(temp,(char*)&max, sizeof(max));
    sendData(uartData, SIGNAL_CTRL_SNDR, temp, 4);
    
    if(valorResistor=100){
        auxResist = valorResistor - 10;
        softPwmWrite(RESIST, auxResist);
    }else{
        auxResist = valorResistor;
        softPwmWrite(RESIST, auxResist);
    }


    printf("Valor Resistor: %d\n", auxResist);

}

void setVent(float valorVentoinha){
    int max = -100;
    char temp[4];
    int auxVen=-100;

    memcpy(temp, (char*)&max, sizeof(max));
    sendData(uartData, SIGNAL_CTRL_SNDR, temp, 4);

    valorVentoinha *= -1;

    if (valorVentoinha <= 40 ){
        auxVen = 40;
        softPwmWrite(VENT, auxVen);
    }else {
        softPwmWrite(VENT,valorVentoinha);
        auxVen = valorVentoinha;
    }
    printf("PWM Ventoinha %d\n",auxVen);
}

void *controlePID(){
    int PIDflag = -1;
    int estavel = 0;
    int tempdiffRI = 0;
    int tempdiffIR = 0;

    while(1){
        while(forno){
            usleep(1000000);
            pid_atualiza_referencia(tempRef);
            PIDflag = pid_controle(tempInt);
            tempdiffRI = tempRef - tempInt;
            tempdiffIR = tempInt - tempRef;

            if( tempdiffRI <= 0.5 && tempdiffRI >= -0.5 && estavel == 5){
                forno = 0;
            }
            else if(PIDflag > 0){
                estavel = 0;
                setResist(PIDflag);
            }
            else if(PIDflag < 0){
                estavel = 0;
                setVent(PIDflag);
            }
            if( tempdiffIR <= 0.5 && tempdiffIR >= -0.5){
                estavel++;
            }
        }
    }
}

void *controleDash(){
    int comando;
    char buffer[40];
    char sendBuffer[4];

    while(1){
        usleep(500000);
        reqData(uartData, USR_CMD);
        usleep(1000000);
        readData(uartData, buffer, 9);
        memcpy(&comando, &buffer[3], 1);

        printf("Comando de Usuario: %x\n", comando);
        switch(comando){
            case 0xA1:
                printf("Ligando o forno (0xA1)\n");
                estado = 1;
                memcpy(sendBuffer, (char*)&estado,sizeof(estado));
                sendData(uartData, SIS_STATE_SNDR, sendBuffer, 1);
                usleep(1000000);
                readData(uartData,buffer, 9);
                break;
            
            case 0xA2:
                printf("Desligando o forno (0xA2)\n");
                forno = 0;
                estado = 0;
                memcpy(sendBuffer,(char*)&estado,sizeof(estado));
                sendData(uartData, SIS_STATE_SNDR, sendBuffer, 1);
                usleep(1000000);
                readData(uartData, buffer, 9);
                break;
            
            case 0xA3:
                printf("Aquecendo do forno (0xA3)\n");
                if(forno){
                    printf("Mudando o valor da resistencia\n");
                    forno = 1;
                    memcpy(sendBuffer, (char*)&forno, sizeof(forno));
                    sendData(uartData,BEHAVIOR_MODE_SNDR, sendBuffer,1);
                    usleep(1000000);
                    readData(uartData, buffer, 9);
                }
                break;

            case 0xA4:
                printf("Cancelando o processo (0xA4)\n");
                if(forno){
                    printf("Parando a ativdade\n");
                    forno = 0;
                    
                    memcpy(sendBuffer, (char*)&forno, sizeof(forno));
                    sendData(uartData, BEHAVIOR_MODE_SNDR, sendBuffer, 1);
                    usleep(1000000);
                    readData(uartData, buffer, 9);
                    
                    softPwmWrite(RESIST,0);
                    softPwmWrite(VENT,0);
                }
                break;
        }
    }
}

int main(int argc, char *argv[]){
    setupProgram();
    signal(SIGINT, trataSinal);
    pthread_create(&t_controleDash, NULL, controleDash, NULL);
    pthread_create(&t_controlePID, NULL, controlePID, NULL);
    getTemp();
    pthread_join(t_controleDash,NULL);
    pthread_join(t_controlePID, NULL);
    return 0;
}